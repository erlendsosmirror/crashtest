/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int nullptr (char **argv)
{
	SimplePrint ("Testing access violation\n");

	int a = 42;
	volatile int *a1 = 0;
	volatile int *a2 = &a;

	if (*a1 == *a2)
		SimplePrint ("Failed: Got equal\n");
	else
		SimplePrint ("Failed: Got unequal\n");

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Usage
///////////////////////////////////////////////////////////////////////////////
int PrintUsage (void)
{
	SimplePrint (
		"Usage: crashtest [options]\n"
		"  nullptr\n"
		"    Test access to 0x0\n"
		"  --help\n"
		"    Print this message\n");
	return 0;
}

int PrintUnknownCommand (void)
{
	SimplePrint (
		"Invalid argument, use\n"
		"   crashtest --help\n"
		"for a usage list\n");
	return 1;
}


///////////////////////////////////////////////////////////////////////////////
// Main function
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	// If no arguments, print default
	if (argc == 1)
		return PrintUnknownCommand ();

	// Check arguments
	if (!strcmp (argv[1], "nullptr"))
		return nullptr (argv);
	else if (!strcmp(argv[1], "--help"))
		return PrintUsage ();
	else
		return PrintUnknownCommand ();
}
